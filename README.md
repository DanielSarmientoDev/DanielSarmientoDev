<h2 align="center">You can reach me at</h2>

<p align="center">
  <a href="https://www.linkedin.com/in/danielstevensarmiento/">
    <img src="https://www.vectorlogo.zone/logos/linkedin/linkedin-icon.svg" alt="Daniel Sarmiento LinkedIn Profile" height="30" width="30">
  </a>

  <a href="https://gitlab.com/DanielSarmientoDev">
    <img src="https://www.vectorlogo.zone/logos/gitlab/gitlab-icon.svg" alt="Daniel Sarmiento GitLab Profile" height="30" width="30">
  </a>
  
  <a href="https://danielsarmientodeveloper.medium.com/">
    <img src="https://www.vectorlogo.zone/logos/medium/medium-tile.svg" alt="Daniel Sarmiento Medium Profile" height="30" width="30">
  </a>
  
  <a href="https://www.youtube.com/c/DanielSarmientoDev">
    <img src="https://www.vectorlogo.zone/logos/youtube/youtube-icon.svg" alt="Daniel Sarmiento Dev YouTube Channel" height="30" width="30">
  </a>
</p>

<h2 align="center">My stack :man_technologist:</h2>

<p align="center">Tools that I use on a daily basis, or that I've used or worked (either much or a bit) with on the past</p>
<p align="center">
  <a href="https://stackshare.io/danielsarmientodev/my-stack">
    <img src="http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat" alt="AnhellO :: StackShare" />
  </a>
</p>

<h2 align="center">Github stats :bar_chart:</h2>

<h4 align="center">Visitor's count :eyes:</h4>

<p align="center"><img src="https://profile-counter.glitch.me/{DanielSarmientoDev}/count.svg" alt="AnhellO :: Visitor's Count" /></p>

<h4 align="center">Top langs :tongue:</h4>

<p align="center"><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=DanielSarmientoDev&langs_count=10&theme=tokyonight&layout=compact" alt="AnhellO :: Top Langs" /></p>

<h4 align="center">Profile stats :musical_keyboard:</h4>

<p align="center"><img src="https://github-readme-stats.vercel.app/api?username=DanielSarmientoDev&show_icons=true&theme=synthwave" alt="Daniel Sarmiento Dev :: Profile Stats" /></p>



